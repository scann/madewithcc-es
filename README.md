# Traducción al español de «Made With Creative Commons»

Este repositorio hospeda el proyecto de traducción al español del
libro [Made With Creative Commons](https://madewith.cc/), de Paul
Stacey y Sarah Hinchliff Pearson.

La traducción fue iniciada por [Leo Arias](https://elopio.net) y
[Gunnar Wolf](https://gwolf.org); te invitamos a participar si puedes
contribuir con nosotros.
